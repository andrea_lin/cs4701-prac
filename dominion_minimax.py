import sys, string, os, subprocess

default_model_name = "trained_classifier"
default_results_filename = "results.txt"

#takes int array and puts it in svm light formatted string
def array_to_svmlight(array):
  #no label given so use 0 as label
  vector_str = "0"
  for i in range(0, len(array)):
    vector_str += (" %i:%d" %(i+1, float(array[i])))
  return vector_str


class MinimaxGame:
  def __init__(self):
    self.gameStarted = False
    self.num_turns = 1 #how many turns we are into the game
    self.is_my_turn = True #if it is my turn or the opponents turn
    self.classifier_name = "svm_classify"
    self.model_name = default_model_name
    self.example_filename = "testpoint.dat"
    self.results_filename = default_results_filename

    self.feature_vector = []
    for i in range(0,31): #31 features
      self.feature_vector.append(0) #zero out vector to start 

  def miniMax(self):
    pass

  #prompt user to input the result of a move, update feature vector accordingly
  def getMove(self):
    if (self.is_my_turn):
      prompt_string = "What cards did you buy?"
    else:
      prompt_string = "What cards did the opponent buy?"
    while(1):
      break
    pass #to finish...



  def runClassifier(self, feature_vectors):
    #use subprocess to run svm light classifier against feature vector
    
    example_file = open(self.example_filename, 'w')
    for i in range(0, len(feature_vectors)):
      vector_str = array_to_svmlight(feature_vectors[i])
      example_file.write(vector_str + "\n")
    example_file.close() #flush write

    #call svm_classify
    subprocess.check_call(["./svm_classify", self.example_filename, 
      self.model_name, self.results_filename])

    return #results in self.results_filename file


def mainloop():
  #start a new game
  while(1):
    u_input = raw_input("start a new game? [y/n]: ")
    if u_input == 'y':
      break #start game
    elif u_input == 'n':
      return 1 #exit from mainloop
    else:
      continue #keep prompting user
  
  #check to see if user wants to start a game
  game = MinimaxGame()

  #figure out whose turn it is
  while(1):
    u_input = raw_input("is it your turn? [y/n]: ")
    if u_input == 'y':
      game.is_my_turn = True
      print "ok, it is your turn"
      break
    elif u_input == 'n':
      game.is_my_turn = False
      print "ok, it is your opponents turn"
      break


  #print out a list of possible cards to buy

  #read in console

  print "done"
  return 0 #game over

exit = 0
while(not(exit)):
  exit = mainloop()